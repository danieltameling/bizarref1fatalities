const { EleventyHtmlBasePlugin } = require("@11ty/eleventy");

module.exports = (eleventyConfig) => {
    eleventyConfig.addPlugin(EleventyHtmlBasePlugin, {
	// The base URL: defaults to Path Prefix
	// baseHref: eleventyConfig.pathPrefix,

	// But you could use a full URL here too:
	// baseHref: "http://example.com/"

	// Comma separated list of output file extensions to apply
	// our transform to. Use `false` to opt-out of the transform.
	extensions: "html",

	// Rename the filters
	filters: {
	    base: "htmlBaseUrl",
	    html: "transformWithHtmlBase",
	    pathPrefix: "addPathPrefixToUrl",
	},
    });
    eleventyConfig.addPassthroughCopy("style.css");
};
