# Bizarre Fatal Accidents in Formula One

Over the years I have come across a number of surreal accidents in the history of Formula One. I knew that in the olden days the security standards were ridiculous. Straw bales were used as barriers, leather caps as head protection and the cars themselves were death traps. However, even with these lower standards some of the fatalities were frankly ridiculous.

For instance, the first time I heard that a driver hit a ditch I was shocked. What??? Why is there a ditch next to a race track? No surprise he died in a time without roll bars. When creating this list I learned that a total of four drivers met their end that way. And that was not even the most outlandish way to pass away. The list also includes two decapitations, one by barbed wire, and somebody killed by a fire extinguisher. I compiled the list to document these freakish deaths that mostly were -- quite frankly -- completely unnecessary.

My main source was Wikipedia and most accidents were neither caught on film nor investigated according to modern standards. So what follows might not be an accurate description of what really happened or what is current consensus.

## Carl Scarborough, 1953 Indianapolis 500

Succumbed to heat exhaustion. In the early days of Formula One, the Indy 500 was part of the calendar. This time, the race took place on a particularly hot day. During a pit stop, he was unable to continue. He was brought to the field hospital with a body temperature of 40 °C (104 °F). He died soon after. Several other drivers also had issues due to the extreme heat.

## Onofre Marimón, 1954 German Grand Prix

He drove into a ditch and hit a tree. His car somersaulted several times and ended up upside down, with him pinned underneath his car.

## Mario Alborghetti, 1955 Pau Grand Prix

Approaching a corner he hit the throttle instead of the brakes and crashed into straw bales. The car came to a halt on top of some of the bales with the throttle fully engaged and the pilot unconscious. He sustained fatal chest injuries, probably from the steering wheel.

His car, as it was apparently common for Italian cars of the time, had the throttle in the middle and the brakes on the right, which caused the confusion.

## Luigi Musso, 1958 French Grand Prix

Another one to drive into a ditch. His car also somersaulted and he didn't survive the accident.

## Peter Collins, 1958 German Grand Prix

Also drove into a ditch. But this time, he was thrown out of the car and hit a tree. He succumbed the following night due to the resulting head injuries.

## Bob Cortner, 1959 Indianapolis 500

It is suspected that a gust of wind caused him to loose control of his car. In the accident his face hit the steering wheel head on, which proved to be fatal.

## Harry Schell, 1960 International Trophy event at Silverstone

In a practise session, he ran wide into the mud beside the track. His car somersaulted and crashed into a brick wall, which collapsed onto him. He did not survive that.

## Chris Bristow, 1960 Belgian Grand Prix

He left the track into an embankment, which flipped his car. He was thrown out of the vehicle straight into barbed wire and decapitated by it.

## Alan Stacey, 1960 Belgian Grand Prix

During the same race, a bird flew straight into his face while he was driving at 190 km/h (120 mph). There is the possibility that this knocked him unconscious or was already fatal. His car then left the track, went through ten feet of hedges, ended up in a field, caught fire and burned down completely. After that he was definitely dead.

## Giulio Cabianca, 1961 Modena Autodrome test track

It is suspected that he suffered a gearbox failure. He was unable to slow down and left the track. A gate was open to allow construction workers to access the circuit and Cabianca used that as an emergency exit. However, this led him straight onto the Via Emilia, a very busy road. He hit a spectator before even passing the gate, and thereafter a bicycle, a motorcycle, a minivan and three park cars before being stopped by the wall of a coachbuilder shop.

The wrecked car was still in fourth gear, which led to the theory of the gearbox failure. A mechanic later said that the gearbox was unsuited for such a powerful car. In addition to Cabianca himself, who passed away three hours after the accident, this was fatal for the motorcyclist, the driver of the minivan and the cyclist, which was hit by the cargo on board the minivan. The spectator miraculously survived.

## Gary Hocking, 1962 Natal Grand Prix

Originally racing motorcycles, he decided to switch to cars thinking that would be safer. During a practice session in his debut race, his car left the track, drove into a ditch, started somersaulting, and came to a halt by crashing into tree stump. The trees had recently been cut down, but the stumps were left there, one of them becoming the final resting place for Hocking's vehicle.

During the whole ordeal, Hocking suffered fatal head injuries. Because of the way the car left the track, it has been surmised that the accident happened due to a medical emergency. Alternatively, a mistake by the driver or a  mechanical defect have been suggested.

## Lorenzo Bandini, 1967 Monaco Grand Prix

He grazed a guard rail, lost control of the car, and struck a light pole only weakly protected by straw bales, which flipped the car. The car caught fire with Bandini still inside. The flames were fueled by the gasoline from the car -- and straw from even more bales. There is also reports that they were fanned by a TV helicopter trying to get closeup footage of the burning wreck.

The marshals struggled with the fire and it took a few minutes to recover the unconcious Bandini from the car, at which point he was severly burned. He died three days later. After this accident straw bales were finally banned from Formula One. One year later, the light pole was behind a guard rail.

## Piers Courage, 1970 Dutch Grand Prix

He left the track at high speed, smashed through a fence, went into the dunes of Zandvoort, rebounded, penetrated the same fence at a different location, returning to the track, where he only came to a halt after a significant distance. The latter part of the crash was obscured by a sand cloud, so one can only speculate whether it was a wheel, a part of the suspension or a fence post that hit Courage in the head with such force that it ripped his helmet off. Witnesses only saw that the wheel and the helmet departed the cloud simultaneously. The car also had caught fire, possibly when the engine separated from the chassis. As the crash was so violent, Courage was probably already dead when the car came to a standstill and wasn't killed by the fire.

The site of the accident quickly turned into an inferno as the chassis used magnesium. The marshals were totally unequipped to deal with any fires and certainly one that burned so hot. They had to wait for the local fire fighters, which were hardly better equipped to deal with a magnesium fire. It took them hours to get the flames under control, and ultimately, they covered the wreck with sand to extinguish the flames. At this point Courage body was still inside. To add insult to injury, the track announcer initially stated that Courage had survived the accident unscathed.

Even before the race the circuit was notorious for outdated safety standards and the Dutch Grand Prix was skipped in 1972 because the drivers refused to drive there until the situation was improved.

## Helmuth Koinigg, 1974 US Grand Prix

Suffering from a suspension failure, he hit straight into a guard rail at relatively low speed. The guard rail, however, wasn't installed properly: the lower band gave way and let the car pass through. But the upper bands held firm, decapitating Koinigg. If the guard rail had been installed properly, he would have remained unscathed.

## Mark Donohue, 1975 Austrian Grand Prix

After a tire blew during a practice session in the fastest corner of the track, his car went right through several catch fences, over a guard rail and through several billboards. Debris from his car killed a marshal and injured another one seriously.

Donohue appeared to be unharmed, but soon developed a headache and died after falling into a coma from a cerebral hemorrhage. It is suspected that his head hit a pole from one of the fences or one of the billboards.

## Tom Pryce, 1977 South African Grand Prix

Pryce's team mate Renzo Zorzi had to drop out of the race and parked the car next to the track. The vehicle caught fire while Zorzi was struggling to leave the vehicle. Two marshals saw this and made their way across the track to quell the emerging fire. The problem was they were just behind a crest, making it impossible for the drivers that were still competing to see them. Hans-Joachim Stuck barely managed to avoid them, but Tom Pryce wasn't so lucky. He hit one of the marshals at 270 km/h (170 mph), who died instantly, being thrown high into the air. His body was unrecognizable and he was only identified by gathering all marshals after the race and looking who was missing.

To deal with the fire, this marshal had been carrying a 18 kg (40 lbs) fire extinguisher. The extinguisher hit Pryce right in the head, went flying over the grandstand next to the track into a car park. Tom Pryce didn't survive the impact, but his car continued. It veered off track, barrelled down the narrow strip next to it without slowing down, until it had to cross the tarmac again at the end of the straight. Since the strip was a little higher than the road itself, it literally flew back onto the track, landing right on top of Jacques Laffite's car, still at high speed. Luckily, it landed on the front wing; had Laffite been a little further along, this could have been fatal for him as well.

The vehicles then went off into a bunch of catch fences, which managed to bring them to a halt. Despite heavy damage to his car, Laffite remained unharmed. Initially, he was furious about what just had occurred, but soon had to realize that something terrible had happened to the other driver.
