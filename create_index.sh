#!/usr/bin/bash

echo '---'
echo 'layout: default'
head -n 1 README.md | cut -b 2- | xargs echo 'title:'
echo '---'
tail -n +2 README.md
